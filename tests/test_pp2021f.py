from pp2021f import __version__
from pp2021f.mastermind import (
    visszajelzes,
    generate_all_cases,
    code_2_string,
    create_vector,
)


def test_same():
    assert visszajelzes([0, 1, 2, 3], [0, 1, 2, 3]) == (4, 0)


def test_function_pure():
    guess = [1, 1, 3, 1]
    code = [1, 2, 3, 4]
    assert visszajelzes(guess, code) == (2, 0)
    assert code == [1, 2, 3, 4]


def test_mindenfele():
    assert visszajelzes([1, 2, 3, 4], [1, 5, 4, 3]) == (1, 2)
    assert visszajelzes([1, 1, 3, 3], [5, 5, 5, 5]) == (0, 0)


def test_symmetry():
    assert visszajelzes([1, 1, 3, 1], [1, 2, 3, 4]) == visszajelzes(
        [1, 2, 3, 4], [1, 1, 3, 1]
    )


def test_zero_vector():
    assert create_vector(2) == [0, 0]
    assert create_vector(1) == [0]
    assert create_vector(4) == [0, 0, 0, 0]


def test_generate_all_cases_1():
    assert sorted(generate_all_cases(2, 2)) == sorted([[0, 0], [1, 0], [0, 1], [1, 1]])
    assert len(generate_all_cases(6, 4)) == 6 ** 4
    assert len(set(map(code_2_string, generate_all_cases(6, 4)))) == 6 ** 4


def test_code_2_string():
    assert code_2_string([1, 2, 3, 4]) == "1234"

