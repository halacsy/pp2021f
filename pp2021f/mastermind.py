import itertools


def visszajelzes(x, y):
    talalgatas = x.copy()
    kod = y.copy()
    fekete = 0
    feher = 0
    for x in range(len(kod)):
        if talalgatas[x] == kod[x]:
            fekete += 1
            talalgatas[x] = "volt"
            kod[x] = "ez is"

    for x in range(len(kod)):
        if talalgatas[x] in kod:
            feher += 1
            kod.remove(talalgatas[x])

    return (fekete, feher)


# Fruzsi
def variaciok(m, n):
    lista = [[]]
    for i in range(n):
        ujlista = []
        for elem in lista:
            for i in range(m):
                ujlista.append(elem + [i])
        lista = ujlista
    return lista


def create_vector(dimension, init=0):
    current = list()
    for i in range(0, dimension):
        current.append(init)

    return current


def for_loop_version(n):
    combinations = []
    for w in range(n):
        for x in range(n):
            for y in range(n):
                for z in range(n):
                    combinations.append([w, x, y, z])
    return combinations


# HP version


def generate_all_cases(number_of_colors, number_of_positions):
    result = []
    current = create_vector(number_of_positions, 0)
    max = create_vector(number_of_positions, number_of_colors - 1)

    while current != max:
        result.append(current)
        current = add_one(current, number_of_colors)
    result.append(current)

    return result


def add_one(x, number_system):
    remain = 1
    # we copy the input
    result = [v for v in x]
    current_digit = 0
    while True:
        if x[current_digit] + 1 >= number_system:
            result[current_digit] = x[current_digit] + 1 - number_system
            remain = 1
            current_digit += 1
        else:
            result[current_digit] = x[current_digit] + 1
            return result


generate_all_cases = variaciok


def code_2_string(code):
    return "".join(map(str, code))

