# how to install the code
Python can have a messy environment if you use libraries. You installa a version
of a library on your computer, I install a different version and then we cannot
work on the same code base.

That's why poetry is invented. Read this
https://andrewbrookins.com/python/why-poetry/

So you need `poetry` to run the code. First install it. 
https://python-poetry.org/docs/

Then go to the directory where `README.rst` sits, open a terminal and
```
poetry install
poetry shell
pytest
```

If you can see that the tests are OK then you are good. The package is installed. 
You can open your favorite editor to edit the code.

Whenever you are done just run
```
pytest
```

## How the code is organized
The core code sits in `pp2021f` which is (python practice 2021 fall). We add the _smart_ 
functions to `mastermind.py` file.

Tests go to `tests/*`. Tests are only runned if the file name starts with `test_`



