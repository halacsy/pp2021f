from pp2021f.mastermind import generate_all_cases, visszajelzes
import random
code = [3, 4, 3, 2]

all_possible_codes = generate_all_cases(6, 4)

print(len(all_possible_codes))

i = 0
while(True):
    i += 1
    guess = random.choice(all_possible_codes)
    
    clue = visszajelzes(guess, code)
    print(i," ", len(all_possible_codes), " ", guess, " ", clue)
    if clue == (4, 0):
        break
    
    all_possible_codes = [elem for elem in all_possible_codes if visszajelzes(elem, guess) == clue]
    # for elem in all_possible_codes.copy():
    #    if visszajelzes(elem, guess) != clue:
    #        all_possible_codes.remove(elem)
            
print(i)
print(guess)
    


